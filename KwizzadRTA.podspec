Pod::Spec.new do |s|  
    s.name              = 'KwizzadRTA'
    s.version           = '0.0.2'
    s.summary           = 'KwizzadRTA'
    s.homepage          = 'https://tvsmiles.de/'

    s.author            = { 'Name' => 'you@yourcompany.com' }
   # s.license           = { :type => 'MIT', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :http => 'https://rta-kit.s3.eu-central-1.amazonaws.com/KwizzadRTA-0.0.2.tar.gz?versionId=null' }
    s.ios.deployment_target = '9.0'
    s.resources = "KwizzadRTA.framework/*.storyboardc"
    s.ios.vendored_frameworks = 'KwizzadRTA.framework'
    s.frameworks  = 'SafariServices', 'WebKit', 'AdSupport'

     s.subspec 'Core' do |ss|
        ss.dependency 'GoogleAds-IMA-iOS-SDK', '3.9.0'
     end
     
     s.subspec 'Ogury' do |ss|
       ss.dependency 'AdinCube/Core', '2.6.6'
       ss.dependency 'AdinCube/Amazon', '2.6.6'
       ss.dependency 'AdinCube/Tapjoy', '2.6.6'
       ss.dependency 'AdinCube/Chartboost', '2.6.6'
     end
     
     s.subspec 'FyberSDK' do |ss|
       ss.dependency 'FyberSDK', '8.22.0'
     end
     
     s.subspec 'Admob' do |ss|
       ss.dependency 'Google-Mobile-Ads-SDK', '7.50.0'
     end

     s.subspec 'Aerserv' do |ss|
       ss.dependency 'aerserv-ios-sdk'
     end

     s.subspec 'OpenBidSDK' do |ss|
       ss.dependency 'OpenBidSDK', '1.2.0'
     end
     
     s.subspec 'FBAudienceNetwork' do |ss|
       ss.dependency 'FBAudienceNetwork', '5.5.1'
     end
     
     s.subspec 'FeedAd' do |ss|
       ss.dependency 'FeedAd', '1.1.6'
     end
     
     s.subspec 'SpotX' do |ss|
       ss.dependency 'SpotX-SDK', '4.2.1'
       ss.dependency 'SpotX-SDK-MOAT', '4.2.1'
     end
     
     s.subspec 'IronSource' do |ss|
       ss.dependency 'IronSourceSDK', '6.8.4'
     end
     
     s.subspec 'AAT' do |ss|
       ss.dependency 'AATKit/Core', '2.68.11'
       ss.dependency 'AATKit/Mopub', '2.68.11'
       ss.dependency 'AATKit/Unity', '2.68.11'
       ss.dependency 'AATKit/AdColony', '2.68.11'
       ss.dependency 'AATKit/SmartAdServer', '2.68.11'
       ss.dependency 'AATKit/Smaato', '2.68.11'
       ss.dependency 'AATKit/Inmobi', '2.68.11'
       ss.dependency 'AATKit/Vungle', '2.68.11'
     end
    
end
